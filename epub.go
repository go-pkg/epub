// Epub reader library.
//
// Bindings from http://sourceforge.net/projects/ebook-tools/
// Only epub_get_ocf_file missing to be API complete.
package epub

/*
#cgo LDFLAGS: -lepub

#include <epub.h>
#include <epub_version.h>
#include <stdlib.h>
#include <string.h>

typedef struct epub epub;
typedef struct eiterator eiterator;
typedef struct titerator titerator;

int length(unsigned char *str) {
	int i;
	for (i = 0; str[i] != 0; i++);
	return i;
}

unsigned char * getElement(unsigned char ** array, int i) {
	return array[i];
}

int libepubIsVersion(int a, int b, int c) {
	return LIBEPUB_IS_VERSION(a, b, c);
}
*/
import "C"

import (
	"errors"
	"unsafe"
)

// epub file
type Epub C.epub

// book iterator
type Eiterator C.eiterator

// book toc iterator
type Titerator C.titerator

const (
	// Metadata types
	EPUB_ID          = C.EPUB_ID          // ebook id
	EPUB_TITLE       = C.EPUB_TITLE       // ebook title
	EPUB_CREATOR     = C.EPUB_CREATOR     // ebook creator
	EPUB_CONTRIB     = C.EPUB_CONTRIB     // ebook contributor
	EPUB_SUBJECT     = C.EPUB_SUBJECT     // ebook subject
	EPUB_PUBLISHER   = C.EPUB_PUBLISHER   // ebook publisher
	EPUB_DESCRIPTION = C.EPUB_DESCRIPTION // ebook description
	EPUB_DATE        = C.EPUB_DATE        // ebook data 
	EPUB_TYPE        = C.EPUB_TYPE        // ebook type 
	EPUB_FORMAT      = C.EPUB_FORMAT      // ebook format 
	EPUB_SOURCE      = C.EPUB_SOURCE      // ebook source 
	EPUB_LANG        = C.EPUB_LANG        // ebook language 
	EPUB_RELATION    = C.EPUB_RELATION    // ebook relation
	EPUB_COVERAGE    = C.EPUB_COVERAGE    // ebook coverage
	EPUB_RIGHTS      = C.EPUB_RIGHTS      // ebook rights 
	EPUB_META        = C.EPUB_META        // ebook extra metadata

	//  Ebook Iterator types
	EITERATOR_SPINE     = C.EITERATOR_SPINE     // all the spine 
	EITERATOR_LINEAR    = C.EITERATOR_LINEAR    // the linear reading spine parts 
	EITERATOR_NONLINEAR = C.EITERATOR_NONLINEAR // the non linear reading spine parts 

	// Ebook Table Of Content Iterator types
	TITERATOR_NAVMAP = C.TITERATOR_NAVMAP // Navigation map  
	TITERATOR_GUIDE  = C.TITERATOR_GUIDE  // Guide to the ebook parts 
	TITERATOR_PAGES  = C.TITERATOR_PAGES  // The pages of the ebook 

	// libepub version
	LIBEPUB_VERSION_STRING  = C.LIBEPUB_VERSION_STRING
	LIBEPUB_VERSION_MAJOR   = C.LIBEPUB_VERSION_MAJOR
	LIBEPUB_VERSION_MINOR   = C.LIBEPUB_VERSION_MINOR
	LIBEPUB_VERSION_RELEASE = C.LIBEPUB_VERSION_RELEASE
	LIBEPUB_VERSION         = C.LIBEPUB_VERSION
)

// Check if the libepub version is greater or equal than a.b.c
func IsVersion(a, b, c int) bool {
	if C.libepubIsVersion(C.int(a), C.int(b), C.int(c)) == 0 {
		return false
	}
	return true
}

// This function accepts an epub filename. It then parses its information and
// returns it as an epub struct.
//
// filename is the name of the file to open.
// debug is the debug level (0=none, 1=errors, 2=warnings, 3=info).
// Returns the reference to an epub type and nil error or a nil reference
// and an error
func Open(name string, debug int) (*Epub, error) {
	e := (*Epub)(C.epub_open(C.CString(name), C.int(debug)))
	if e == nil {
		return nil, errors.New("Can not open the file")
	}
	return e, nil
}

// This function sets the debug level to the given level
// (0=none, 1=errors, 2=warnings, 3=info)
func (e *Epub) Debug(debug int) {
	C.epub_set_debug((*C.epub)(e), C.int(debug))
}

// Debugging function dumping various file information.
func (e *Epub) Dump() {
	C.epub_dump((*C.epub)(e))
}

// TODO: epub_get_ocf_file

// Get a metadata field
func (e *Epub) Metadata(field uint32) []string {
	var size C.int
	var meta **C.uchar = C.epub_get_metadata((*C.epub)(e), field, &size)
	defer C.free(unsafe.Pointer(meta))

	gmeta := []string{}
	var i C.int
	for i = 0; i < size; i++ {
		var cstr *C.uchar = C.getElement(meta, i)
		defer C.free(unsafe.Pointer(cstr))

		var length C.int = C.length(cstr)
		gostr := C.GoBytes(unsafe.Pointer(cstr), length)
		gmeta = append(gmeta, string(gostr))
	}
	return gmeta
}

// Returns the content of a give filename in the epub.
// (Useful for getting book files). 
func (e *Epub) Data(name string) []byte {
	var cdata *C.char
	length := C.epub_get_data((*C.epub)(e), C.CString(name), &cdata)
	if length <= 0 {
		return nil
	}
	return C.GoBytes(unsafe.Pointer(cdata), length)
}

// Close an epub file
func (e *Epub) Close() error {
	if C.epub_close((*C.epub)(e)) != 0 {
		return errors.New("There was a problem closing the epub")
	}
	return nil
}

// Returns a book iterator of the requested type
// for the given epub struct.
//
// itType can be: EITERATOR_SPINE, EITERATOR_LINEAR or EITERATOR_NONLINEAR
func (e *Epub) Iterator(itType uint32) *Eiterator {
	it := (*Eiterator)(C.epub_get_iterator((*C.epub)(e), itType, 0))
	return it
}

// Returns a book toc iterator of the requested type
// for the given epub struct.
//
// titType can be: TITERATOR_NAVMAP, TITERATOR_GUIDE or TITERATOR_PAGES
func (e *Epub) Titerator(titType uint32) *Titerator {
	tit := (*Titerator)(C.epub_get_titerator((*C.epub)(e), titType, 0))
	return tit
}

// updates the iterator to the next element and returns
// the data or error.
func (it *Eiterator) Next() (string, error) {
	var data *C.char = C.epub_it_get_next((*C.eiterator)(it))
	var err error = nil
	if data == nil {
		return "", errors.New("It's the last item")
	}
	return char2String(data), err
}

// Returns the iterator's data.
func (it *Eiterator) Curr() string {
	var data *C.char = C.epub_it_get_curr((*C.eiterator)(it))
	return char2String(data)
}

// Returns a string with the url of the iterator's current data. 
func (it *Eiterator) CurrUrl() string {
	var data *C.char = C.epub_it_get_curr_url((*C.eiterator)(it))
	return char2String(data)
}

// Close the iterator
func (it *Eiterator) Close() {
	if it != nil {
		C.epub_free_titerator((*C.eiterator)(it))
	}
}

// Returns if the current entry is valid.
func (t *Titerator) Valid() bool {
	return C.epub_tit_curr_valid((*C.titerator)(t)) != 0
}

// Returns the depth of the toc iterator's current entry. 
func (t *Titerator) Depth() int {
	return int(C.epub_tit_get_curr_depth((*C.titerator)(t)))
}

// Returns the link of the toc iterator's current entry. 
func (t *Titerator) Link() string {
	var data *C.char = C.epub_tit_get_curr_link((*C.titerator)(t))
	return char2String(data)
}

// Returns the label of the toc iterator's current entry. 
func (t *Titerator) Label() string {
	var data *C.char = C.epub_tit_get_curr_label((*C.titerator)(t))
	return char2String(data)
}

// updates the iterator to the next element.
//
// return if success.
func (t *Titerator) Next() bool {
	return C.epub_tit_next((*C.titerator)(t)) != 0
}

// Close the iterator
func (t *Titerator) Close() {
	if t != nil {
		C.epub_free_titerator((*C.titerator)(t))
	}
}

// Cleans up after the library. Call this when you are done with the library.
func Cleanup() {
	C.epub_cleanup()
}

func char2String(str *C.char) string {
	if str == nil {
		return ""
	}
	length := C.strlen(str)
	return string(C.GoBytes(unsafe.Pointer(str), C.int(length)))
}
